#!/usr/bin/env python

import httpx
from rich.console import Console
from rich.prompt import Prompt, Confirm
import typer
import os
import pyfiglet

console = Console()


class Absen:
    base_url = "https://smkn7smgv2.fresto.biz/smk_smg7n/"
    file = {}

    def __init__(self, base_url=None) -> None:
        if base_url:
            self.base_url = base_url

        self.client = httpx.Client(base_url=self.base_url)

    def upload_file(self, path_file):
        if not os.path.exists(path_file):
            console.print(f"[red][FAIL][/red] FILE NOT FOUND")
            exit()

        path = "https://contentgen2.fresto.biz/upload_content.php"
        files = {"file": open(path_file, "rb")}
        data = {
            "kode": "d5077f4b48c5b504d93bc2612a5714eb",
            "scope": "smkn7smg",
            "type": "images",
        }
        res = self.client.post(path, files=files, data=data)
        if res.is_error:
            console.print(f"[red][FAIL][/red] UPLOAD FAILED")
            exit()
        console.print(f"[green][OK][/green] UPLOAD SUCCESS")
        self.file = res.json()
        return self

    def login(self, username, password):
        path = "/home/login"
        data = {"username": username, "password": password, "submit": " Sign in "}
        res = self.client.post(path, data=data)

        if res.is_error or res.status_code != 302:
            console.print(f"[red][FAIL][/red] LOGIN FAILED")
            exit()
        console.print(f"[green][OK][/green] LOGIN SUCCESS")
        return self

    def presensi(self, absen_type="masuk"):
        path = f"/kbm/presensi_aksi/input_presensi/{absen_type}"
        data = {
            "latitude_place": "-6.9910773",
            "longitude_place": "110.4206208",
            f"masuk_latitude": "-6.9910773",
            f"masuk_longitude": "110.4206208",
            f"masuk_jarak": "5",
            f"{absen_type}_url_foto": self.file.get("link"),
            f"{absen_type}_foto": self.file.get("name"),
        }
        res = self.client.post(path, data=data, follow_redirects=True)
        if res.is_error:
            console.print(f"[red][FAIL][/red] ABSEN FAILED")
            exit()
        console.print(f"[green][OK][/green] ABSEN SUCCESS")


def main():
    username = Prompt.ask("Masukkan username")
    password = Prompt.ask("Masukkan password", default=username)
    absen_type = Prompt.ask("Tipe absen", choices=["masuk", "pulang"])
    photo = Prompt.ask("Masukkan foto")

    confirm = Confirm.ask("Yakin, absen?")
    print("")

    if not confirm:
        console.print(f"[red][FAIL][/red] ABSEN CANCELED")
        exit()

    absen = Absen()
    absen.login(username, password)
    absen.upload_file(photo)
    absen.presensi(absen_type)


if __name__ == "__main__":
    console.clear()
    f = pyfiglet.figlet_format("ABSEN", font="isometric3")
    console.print(f, style="bold yellow")
    console.print(
        "Absen STEMBA - v1.0.0", style="italic green", width=70, justify="center"
    )
    console.print(
        "CLI tool untuk bypass absen STEMBA\n\n",
        justify="center",
        width=70,
    )
    typer.run(main)
